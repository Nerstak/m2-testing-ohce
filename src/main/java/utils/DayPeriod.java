package utils;

public enum DayPeriod {
    MORNING,
    AFTERNOON,
    NIGHT;

    /**
     * Convert an hour
     * @param hour Hour on 0-23 range (boundary inclusive)
     * @return Period (Afternoon by default)
     */
    public static DayPeriod hourToPeriod(int hour) {
        if(hour >= 20 && hour <= 23 || hour >= 0 && hour < 6) {
            return NIGHT;
        } else if (hour >= 6 && hour < 12) {
            return MORNING;
        } else {
            return AFTERNOON;
        }
    }
}
