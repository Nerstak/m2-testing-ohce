package utils;

/**
 * Class for any palindrome / reverse actions on String
 */
public class Palindrome {
    /**
     * Check if a string is a palindrome
     * @param s String to check
     * @return Palindrome status
     */
    public static boolean isPalindrome(String s) {
        int stringSize = s.length();
        for (int i = 0; i < stringSize / 2; i++) {
            if(s.charAt(i) != s.charAt(stringSize - (i + 1))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reverse a string
     * @param s String to reverse
     * @return Reversed string
     */
    public static String reverseString(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = s.length() - 1; i >= 0 ; i--) {
            builder.append(s.charAt(i));
        }

        return builder.toString();
    }
}
