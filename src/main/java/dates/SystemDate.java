package dates;

import java.time.LocalDateTime;

/**
 * Date indicator based on system calls
 */
public class SystemDate implements DateIndicator {
    @Override
    public int getTimeOfDay() {
        return LocalDateTime.now().getHour();
    }
}
