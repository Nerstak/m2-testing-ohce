package dates;

/**
 * Interface for date indication
 */
public interface DateIndicator {
    /**
     * Get the time of the day (hour only)
     * @return Hour (24 hours time format)
     */
    int getTimeOfDay();
}
