package dates;

/**
 * Fake implementation of the Date Indicator
 * If an incorrect hour is given in constructor, default value will be 0
 */
public class FakeDateIndicator implements DateIndicator {
    /**
     * Hour to return when interface method is called
     */
    private int hourToReturn;

    public FakeDateIndicator(int hourToReturn) {
        setHourToReturn(hourToReturn);
    }

    /**
     * Set hour to return
     * Any negative number is not taken into account
     * Any number over 23 (excluded) is not taken into account
     * @param hourToReturn Hour that will be return by getTimeOfDay
     */
    public void setHourToReturn(int hourToReturn) {
        if(hourToReturn > 0 && hourToReturn < 24) {
            this.hourToReturn = hourToReturn;
        }
    }

    @Override
    public int getTimeOfDay() {
        return hourToReturn;
    }
}
