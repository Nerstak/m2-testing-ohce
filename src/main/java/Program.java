import dates.DateIndicator;
import dates.SystemDate;
import interactions.ConsoleUI;
import interactions.UserInteraction;
import utils.DayPeriod;

import static utils.Palindrome.isPalindrome;
import static utils.Palindrome.reverseString;

public class Program {
    public static void main(String[] args) {
        if(args.length != 0) {
            Program program = new Program(new SystemDate(), new ConsoleUI(), args[0]);
            program.execute();
        }
        
    }

    /**
     * Object to obtain date
     */
    private final DateIndicator dateIndicator;

    /**
     * Object to deal with IO
     */
    private final UserInteraction userInteraction;

    /**
     * Name of user
     */
    private final String name;

    /**
     * Status of execution of program
     */
    private boolean continueExecution = true;

    public Program(DateIndicator dateIndicator, UserInteraction userInteraction, String name) {
        this.dateIndicator = dateIndicator;
        this.userInteraction = userInteraction;
        this.name = name;
    }


    /**
     * Execute program (greeting and main loop)
     */
    public void execute() {
        greetingMessage(dateIndicator.getTimeOfDay());
        while (continueExecution) {
            handleInput();
        }
    }

    /**
     * Print the correct greeting message
     * @param currentTime Time (hours)
     */
    private void greetingMessage(int currentTime) {
        switch (DayPeriod.hourToPeriod(currentTime)) {
            case NIGHT -> userInteraction.printOutput("¡Buenos noches " + name + "!");
            case MORNING -> userInteraction.printOutput("¡Buenos días " + name + "!");
            case AFTERNOON -> userInteraction.printOutput("¡Buenos tardes " + name + "!");
        }
    }

    /**
     * Handle an input
     */
    public void handleInput() {
        String word = userInteraction.readInput();
        if(word.equals("Stop!")) {
            continueExecution = false;
            userInteraction.printOutput("Adios " + name);
        } else {
            boolean isPalindrome = isPalindrome(word);

            if (isPalindrome) {
                userInteraction.printOutput(word + "\n¡Bonita palabra!");
            } else {
                userInteraction.printOutput(reverseString(word));
            }
        }
    }
}
