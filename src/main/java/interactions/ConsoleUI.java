package interactions;

import java.util.Scanner;

/**
 * User interaction based on system calls
 * Not testable: Testing it would be the same as testing the System class
 */
public class ConsoleUI implements UserInteraction {
    @Override
    public String readInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    @Override
    public void printOutput(String output) {
        System.out.println(output);
    }
}
