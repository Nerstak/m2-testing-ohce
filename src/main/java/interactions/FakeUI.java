package interactions;

import java.util.ArrayList;
import java.util.List;

/**
 * Fake implementation of UserInteraction
 */
public class FakeUI implements UserInteraction {
    /**
     * Message to return when readInput() method is called
     */
    private String readToReturn;

    /**
     * Outputs given to printOutput()
     */
    private final List<String> messages = new ArrayList<>();

    public FakeUI(String readToReturn) {
        this.readToReturn = readToReturn;
    }

    /**
     * Set value to return when readInput() is called
     * @param readToReturn Value to return
     */
    public void setReadToReturn(String readToReturn) {
        this.readToReturn = readToReturn;
    }

    /**
     * Get outputs given to printOutput()
     * @return Messages passed to printOutput()
     */
    public List<String> getMessages() {
        return messages;
    }

    @Override
    public String readInput() {
        return readToReturn;
    }

    @Override
    public void printOutput(String output) {
        messages.add(output);
    }
}
