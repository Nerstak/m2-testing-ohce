package interactions;

/**
 * Interface defining user interaction methods
 */
public interface UserInteraction {
    /**
     * Read a user input
     * @return Value of user input, as String
     */
    String readInput();

    /**
     * Write output
     * @param output Output to write
     */
    void printOutput(String output);
}
