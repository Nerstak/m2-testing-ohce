package dates;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FakeDateIndicatorTest {

    @Test
    void testGetTimeDayCorrectValue() {
        // Arrange
        FakeDateIndicator fdi = new FakeDateIndicator(2);

        // Act
        var hour = fdi.getTimeOfDay();

        // Assert
        assertEquals(hour, 2, "Verify that hour fetch matches the one given");
    }

    @Test
    void testGetTimeDayIncorrectPositiveValue() {
        // Arrange
        FakeDateIndicator fdi = new FakeDateIndicator(40);

        // Act
        var hour = fdi.getTimeOfDay();

        // Assert
        assertEquals(hour, 0, "Verify that hour does not change if incorrect positive value is given");
    }

    @Test
    void testGetTimeDayIncorrectNegativeValue() {
        // Arrange
        FakeDateIndicator fdi = new FakeDateIndicator(-40);

        // Act
        var hour = fdi.getTimeOfDay();

        // Assert
        assertEquals(hour, 0, "Verify that hour does not change if incorrect negative value is given");
    }
}