package dates;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;


class SystemDateTest {
    @Test
    void testBoundaries() {
        // Arrange
        SystemDate sd = new SystemDate();

        // Act
        int currentHour = sd.getTimeOfDay();

        // Assert
        assertTrue(currentHour < 24 && currentHour >= 0, "System date hour is within boundaries");
    }
}