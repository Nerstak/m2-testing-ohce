import dates.FakeDateIndicator;
import interactions.FakeUI;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {
    @Test
    void TestGreetingNight() {
        FakeDateIndicator fakeDateIndicator = new FakeDateIndicator(2);
        FakeUI fakeUI = new FakeUI("Stop!");

        Program p = new Program(fakeDateIndicator, fakeUI, "John");
        p.execute();

        var greeting = fakeUI.getMessages().get(0);
        assertEquals("¡Buenos noches John!", greeting, "Greeting message (night) is correct");
    }

    @Test
    void TestGreetingMorning() {
        FakeDateIndicator fakeDateIndicator = new FakeDateIndicator(8);
        FakeUI fakeUI = new FakeUI("Stop!");

        Program p = new Program(fakeDateIndicator, fakeUI, "John");
        p.execute();

        var greeting = fakeUI.getMessages().get(0);
        assertEquals("¡Buenos días John!", greeting,"Greeting message (morning) is correct");
    }

    @Test
    void TestGreetingAfternoon() {
        FakeDateIndicator fakeDateIndicator = new FakeDateIndicator(12);
        FakeUI fakeUI = new FakeUI("Stop!");

        Program p = new Program(fakeDateIndicator, fakeUI, "John");
        p.execute();

        var greeting = fakeUI.getMessages().get(0);
        assertEquals("¡Buenos tardes John!", greeting, "Greeting message (afternoon) is correct");
    }

    @Test
    void TestActionValidPalindromeOutput() {
        FakeUI fakeUI = new FakeUI("otto");
        Program program = new Program(new FakeDateIndicator(0), fakeUI, "");

        program.handleInput();

        var result = fakeUI.getMessages().get(0);
        assertEquals(result,"otto\n¡Bonita palabra!", "Verify that palindrome makes program display congrats message");
    }

    @Test
    void TestActionInvalidPalindromeOutput() {
        FakeUI fakeUI = new FakeUI("hola");
        Program program = new Program(new FakeDateIndicator(0), fakeUI, "");

        program.handleInput();

        var result = fakeUI.getMessages().get(0);
        assertEquals(result,"aloh", "Verify that incorrect palindrome makes program display inverted word");
    }

    @Test
    void TestActionStopOutput() {
        FakeUI fakeUI = new FakeUI("Stop!");
        Program program = new Program(new FakeDateIndicator(0), fakeUI, "Jules");

        program.handleInput();
        var messages = fakeUI.getMessages();

        var result = messages.get(messages.size() - 1);
        assertEquals(result,"Adios Jules", "Verify that goodbye message is displayed on stop");
    }
}