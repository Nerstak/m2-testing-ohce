package interactions;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FakeUITest {
    @Test
    void testRead() {
        // Arrange
        FakeUI fui = new FakeUI("Jake");

        // Act
        var read = fui.readInput();

        // Assert
        assertEquals(read, "Jake", "Fake read input is taken into account");
    }

    @Test
    void testReadMultipleValues() {
        // Arrange
        FakeUI fui = new FakeUI("Jake");
        fui.setReadToReturn("Julia");

        // Act
        var read = fui.readInput();

        // Assert
        assertEquals(read, "Julia", "Updated fake read input is taken into account");
    }

    @Test
    void testWrite() {
        // Arrange
        FakeUI fui = new FakeUI("");

        // Act
        fui.printOutput("Jake");
        var lastMessage = fui.getMessages().get(0);

        // Assert
        assertEquals(lastMessage, "Jake", "Fake print output is taken into account");
    }

}