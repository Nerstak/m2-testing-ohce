package utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DayPeriodTest {
    @Test
    void testEndNight() {
        DayPeriod period = DayPeriod.hourToPeriod(0);
        assertEquals(period, DayPeriod.NIGHT, "Night period is recognized (end of night)");
    }

    @Test
    void testBeginningNight() {
        DayPeriod period = DayPeriod.hourToPeriod(23);
        assertEquals(period, DayPeriod.NIGHT, "Night period is recognized (end of night)");
    }

    @Test
    void testMorning() {
        DayPeriod period = DayPeriod.hourToPeriod(7);
        assertEquals(period, DayPeriod.MORNING, "Morning period is recognized");
    }

    @Test
    void testAfternoon() {
        DayPeriod period = DayPeriod.hourToPeriod(15);
        assertEquals(period, DayPeriod.AFTERNOON, "Afternoon period is recognized");
    }

    @Test
    void testDefault() {
        DayPeriod period = DayPeriod.hourToPeriod(50);
        assertEquals(period, DayPeriod.AFTERNOON, "Default period is Afternoon");
    }

}