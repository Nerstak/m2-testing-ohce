package utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static utils.Palindrome.isPalindrome;
import static utils.Palindrome.reverseString;

class PalindromeTest {
    @Test
    void TestReverseString() {
        String result = reverseString("hola");

        assertEquals(result, "aloh", "Reverse is functional");
    }

    @Test
    void TestValidPalindrome() {
        var res = isPalindrome("oto");

        assertTrue(res, "Palindrome is recognized");
    }

    @Test
    void TestInvalidPalindrome() {
        var res = isPalindrome("oche");

        assertFalse(res, "Invalid palindrome is recognized");
    }
}